<?php

namespace judahnator\LaravelJsonManipulator\Tests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use judahnator\JsonManipulator\JsonArray;
use judahnator\JsonManipulator\Json;
use judahnator\JsonManipulator\JsonObject;
use judahnator\LaravelJsonManipulator\HasJsonAttributes;
use Orchestra\Testbench\TestCase;

/**
 * Class HasJsonAttributesTest
 * @package judahnator\LaravelJsonManipulator\Tests
 * @covers \judahnator\LaravelJsonManipulator\HasJsonAttributes::guessJsonArgumentName
 * @covers \judahnator\LaravelJsonManipulator\HasJsonAttributes::load_json
 */
final class HasJsonAttributesTest extends TestCase
{
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    protected function setUp(): void
    {
        parent::setUp();
        MyModel::migrate();
    }

    /**
     * @covers \judahnator\LaravelJsonManipulator\HasJsonAttributes::load_json_array
     * @covers \judahnator\LaravelJsonManipulator\HasJsonAttributes::load_json_object
     */
    public function testFetchingDefaults(): void
    {
        $MyModel = MyModel::create();
        $this->assertEquals('[]', (string)$MyModel->json_array);
        $this->assertEquals('{}', (string)$MyModel->json_object);
    }

    /**
     * @covers \judahnator\LaravelJsonManipulator\HasJsonAttributes::load_json_array
     * @covers \judahnator\LaravelJsonManipulator\HasJsonAttributes::load_json_object
     */
    public function testBasicFunctionality(): void
    {
        $MyModel = MyModel::create([
            'json_array' => '["foo","bar"]',
            'json_object' => '{"foo":"bar"}'
        ]);

        $this->assertEquals('foo', $MyModel->json_array[0]);
        $this->assertEquals('bar', $MyModel->json_array[1]);
        $this->assertEquals('bar', $MyModel->json_object->foo);

        $MyModel->json_array[] = 'baz';
        $MyModel->json_object->foo = 'baz';

        $this->assertEquals('["foo","bar","baz"]', (string)$MyModel->json_array);
        $this->assertEquals('{"foo":"baz"}', (string)$MyModel->json_object);
    }

    /**
     * @covers \judahnator\LaravelJsonManipulator\HasJsonAttributes::load_json_array
     */
    public function testCustomAttributeNames(): void
    {
        $MyModel = MyModel::create([
            'json_array' => '["foo", "bar"]'
        ]);
        $this->assertEquals($MyModel->json_array, $MyModel->item_not_in_database);
    }

    /**
     * @covers \judahnator\LaravelJsonManipulator\HasJsonAttributes::load_json_object
     */
    public function testDefaultValues(): void
    {
        $MyModel = new MyModel();
        $this->assertEquals('value', $MyModel->item_with_default_value->default);
    }

    /**
     * @covers \judahnator\LaravelJsonManipulator\HasJsonAttributes::load_json_array
     */
    public function testExceptionForInvalidArray(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('A JSON array could not be found');
        $MyModel = new MyModel(['json_array' => '{}']);
        $MyModel->json_array;
    }

    /**
     * @covers \judahnator\LaravelJsonManipulator\HasJsonAttributes::load_json_object
     */
    public function testExceptionForInvalidObject(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('A JSON object could not be found');
        $MyModel = new MyModel(['json_object' => '[]']);
        $MyModel->json_object;
    }
}

final class MyModel extends Model
{
    use HasJsonAttributes;

    protected $fillable = ['json_array', 'json_object'];
    protected $table = 'mytable';
    public $timestamps = false;

    public function getItemNotInDatabaseAttribute(): JsonArray
    {
        return $this->load_json_array('json_array');
    }

    public function getItemWithDefaultValueAttribute(): Json
    {
        return $this->load_json('new_attribute', '{"default":"value"}');
    }

    public function getJsonArrayAttribute(): JsonArray
    {
        return $this->load_json_array();
    }

    public function getJsonObjectAttribute(): JsonObject
    {
        return $this->load_json_object();
    }

    public function messStuffUp(): void
    {
        $this->load_json('asdf');
    }

    public static function migrate(): void
    {
        Schema::create('mytable', function (Blueprint $table) {
            $table->json('json_array')->nullable();
            $table->json('json_object')->nullable();
        });
    }
}
