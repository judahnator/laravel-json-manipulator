Changelog
=========

v3.0.1
------
Fixing issue where composer wouldn't let you use this library with PHP 8.0.

v3.0.0
------
Version 3!

Not much of note here, the API is all the same with a few exceptions in the underlying library which shouldn't affect the consumers of this library.

**Breaking changes:**

 * The minimum PHP version is now 7.4.
 * The minimum supported Laravel version is now 6.0.
 * The underlying "judahnator/json-manipulator" library has been upgraded to version 3.
   * The major breaking change here is the `JsonBase` class has been renamed to `Json` - the rest of the API is the same.

v2.0.1
------
Laravel 8.x support!
Just a little tweak so only doing a minor release.

v2.0.0
------
**Breaking changes:**

 * This library no longer supports PHP 7.1

**Other changes:**

 * Added tests to ensure PHP 7.4 compatibility, now allows usage with Laravel ^7.0.

v1.0.3
------
Tweaking illuminate/support version constraint, now ^6.0

v1.0.2
------
Adding support for Laravel 6.0

v1.0.1
------
Adding support for Laravel 5.8

v1.0.0
------
Release v1.0.0

* First "stable" release.
* Official PHP 7.3 support.
* Flagging support for specific Laravel versions.

v0.2.0
------
Second beta release.

Added the ability to pass in custom attribute names, just in case your column was not snake_case
You can now pass in a default value to the load_json* functions
You may call the load_json function directly now, which can be handy if you want to skip object or array assertions
Added more documentation

This release should be 100% backwards compatible.

v0.1.0
------
Initial public release