<?php

namespace judahnator\LaravelJsonManipulator;

use Illuminate\Support\Str;
use judahnator\JsonManipulator\JsonArray;
use judahnator\JsonManipulator\Json;
use judahnator\JsonManipulator\JsonObject;
use RuntimeException;
use function judahnator\JsonManipulator\load_json;

trait HasJsonAttributes
{

    /**
     * Given a method name, attempts to guess the attribute desired
     * @param string $callingMethodName
     * @return string
     */
    private static function guessJsonArgumentName(string $callingMethodName): string
    {
        return Str::snake(
            preg_replace(
                '/get(.+)Attribute/',
                '$1',
                $callingMethodName
            )
        );
    }

    /**
     * Loads a generic JSON string with no type checks. Default is an empty JSON object.
     *
     * @param string $attribute
     * @param string $default
     * @return Json
     */
    protected function load_json(string $attribute, string $default = '{}'): Json
    {
        if (!array_key_exists($attribute, $this->attributes) || !$this->attributes[$attribute]) {
            $this->attributes[$attribute] = $default;
        }
        return load_json($this->attributes[$attribute]);
    }

    /**
     * Fetches a JSON array, type enforced.
     *
     * @param string|null $attribute
     * @param string $default
     * @return JsonArray
     */
    protected function load_json_array(string $attribute = null, string $default = '[]'): JsonArray
    {
        $result = $this->load_json(
            $attribute ?: self::guessJsonArgumentName(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1]['function']),
            $default
        );
        if (!$result instanceof JsonArray) {
            throw new RuntimeException('A JSON array could not be found');
        }
        return $result;
    }

    /**
     * Fetches a JSON object, type enforced.
     *
     * @param string|null $attribute
     * @param string $default
     * @return JsonObject
     */
    protected function load_json_object(string $attribute = null, string $default = '{}'): JsonObject
    {
        $result = $this->load_json(
            $attribute ?: self::guessJsonArgumentName(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1]['function']),
            $default
        );
        if (!$result instanceof JsonObject) {
            throw new RuntimeException('A JSON object could not be found');
        }
        return $result;
    }
}
